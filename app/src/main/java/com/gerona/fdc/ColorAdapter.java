package com.gerona.fdc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ColorAdapter extends ArrayAdapter<Color> {

    private Context mContext;
    private List<Color> moviesList;

    public ColorAdapter(@NonNull Context context, List<Color> list) {
        super(context, 0 , list);
        mContext = context;
        moviesList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.item_color,parent,false);

        Color currentMovie = moviesList.get(position);

        TextView color = (TextView) listItem.findViewById(R.id.tv_color);
        color.setText(currentMovie.getName());

        return listItem;
    }
}