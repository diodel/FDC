package com.gerona.fdc;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView lvColors;
    private ColorAdapter adapter;
    private List<Color> colorList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lvColors = (ListView) findViewById(R.id.lv_colors);
        colorList = generateColors();
        adapter = new ColorAdapter(this,colorList);
        lvColors.setAdapter(adapter);

        lvColors.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.e("colorList",colorList.get(i).getName());
                Intent intent = new Intent(getApplicationContext(), ActivityColor.class);
                intent.putExtra("code",colorList.get(i).getCode());
                startActivity(intent);
            }
        });
    }

    private List<Color> generateColors(){
        List<Color> colorList = new ArrayList<>();
        colorList.add(new Color("Green", ContextCompat.getColor(this, R.color.green)));
        colorList.add(new Color("Red", ContextCompat.getColor(this, R.color.red)));
        colorList.add(new Color("Blue", ContextCompat.getColor(this, R.color.blue)));
        return  colorList;
    }
}